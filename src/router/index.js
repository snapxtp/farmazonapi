import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: "/farmazonapi",
      component: Home
    }
  ],
  mode: "history"
});
